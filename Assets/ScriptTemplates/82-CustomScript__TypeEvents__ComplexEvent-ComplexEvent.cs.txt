namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;
	
	public class #TYPE#
	{
	    // To fill...
	}

	[CreateAssetMenu(fileName = "#SCRIPTNAME#", menuName = "Events/#SCRIPTNAME#")]
	public class #SCRIPTNAME# : ParametrisedEvent<#TYPE#> { }
}