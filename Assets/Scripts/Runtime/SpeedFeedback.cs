using Cinemachine;

using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.VFX;

[RequireComponent(typeof(KartController))]
public class SpeedFeedback : MonoBehaviour {
	[Header("Camera FX/post process parameters and objects")]
	public float m_fovBonusPhysicalSpeed = 20.0f;

	public float m_fovBonusVirtualSpeed = 30.0f;
	public float maxLensDistortion = 0.2f;
	public float maxFeedbackVirtualSpeed = 5000;
	public float minimumSpeedForFX;
	public LayerMask speedFxLayer;

	public ParticleSystem trailParticles;
	public AnimationCurve m_factorChromaticAberration;
	public AnimationCurve m_factorLensDistortion;
	public AnimationCurve m_factorVignette;
	public Volume m_cinemachineVolume;
	public AnimationCurve m_factorMotionBlur;

	[Header("Camera objects")]
	//Camera FX
	public VisualEffect speedFX;

	private float currentSpeedNormalized;
	private float currentVirtualSpeedNormalized;
	private bool isFxPlaying;

	//Script parameters and objects
	private ChromaticAberration m_chromaticAberration;
	private KartController m_controller;
	private LensDistortion m_lens;
	private MotionBlur m_motionBlur;
	private PlayerState m_playerState;
	private CinemachineImpulseSource m_shakes;
	private CinemachineVirtualCamera m_vcam;
	private Vignette m_vignette;

	private ParticleSystem.MainModule main;
	
	// PostProcess Profile
	private VolumeProfile m_volume;

	private void Start() {
		m_controller = GetComponent<KartController>();
		m_volume = m_cinemachineVolume.profile;
		m_vcam = GetComponentInChildren<CinemachineVirtualCamera>();
		m_shakes = m_vcam.GetComponent<CinemachineImpulseSource>();
		m_playerState = GetComponent<PlayerState>();
		main = trailParticles.main;
	}

	private void Update() {
		currentVirtualSpeedNormalized = Mathf.InverseLerp(0, maxFeedbackVirtualSpeed,
														  Mathf.Min(m_playerState.m_virtualSpeed,
																	maxFeedbackVirtualSpeed));
		currentSpeedNormalized = m_controller.currentSpeedNormalized;
		PostProcess();

		CameraShake();
	}

	private void CameraShake() {
		if (m_controller.isDrifting || currentVirtualSpeedNormalized > 0.7) {
			m_shakes.GenerateImpulse();
		}
	}

	private void PostProcess() {
		FieldOfViewFeedback();
		ChromaticFeedback();
		DistortionFeedback();
		VignetteFeedback();
		MotionBlurFeedback();
		RocketTrailFeedback();
		UpdateFX();
	}

	private void FieldOfViewFeedback() {
		m_vcam.m_Lens.FieldOfView = 70 + (m_fovBonusPhysicalSpeed * currentSpeedNormalized);
		m_vcam.m_Lens.FieldOfView += m_fovBonusVirtualSpeed * currentVirtualSpeedNormalized;
	}

	private void ChromaticFeedback() {
		if (m_volume.TryGet(out m_chromaticAberration)) {
			m_chromaticAberration.intensity.value =
				m_factorChromaticAberration.Evaluate(currentVirtualSpeedNormalized * currentSpeedNormalized);
		}
	}

	private void DistortionFeedback() {
		if (m_volume.TryGet(out m_lens)) {
			m_lens.intensity.value =
				-m_factorLensDistortion.Evaluate(currentVirtualSpeedNormalized * currentSpeedNormalized);
		}
	}

	private void VignetteFeedback() {
		if (m_volume.TryGet(out m_vignette)) {
			m_vignette.intensity.value =
				m_factorVignette.Evaluate(currentVirtualSpeedNormalized * currentSpeedNormalized) < maxLensDistortion
					? m_factorVignette.Evaluate(currentVirtualSpeedNormalized * currentSpeedNormalized)
					: maxLensDistortion;
		}
	}

	private void MotionBlurFeedback() {
		if (m_volume.TryGet(out m_motionBlur)) {
			m_motionBlur.intensity.value = m_factorMotionBlur.Evaluate(currentVirtualSpeedNormalized);
		}
	}

	private void RocketTrailFeedback() {
		main.startLifetime = 5f * currentSpeedNormalized * currentVirtualSpeedNormalized > 0.7f
			? 5f * currentSpeedNormalized * currentVirtualSpeedNormalized
			: 0.7f;
	}

	private void UpdateFX() {
		if (currentSpeedNormalized > 0.2f && !isFxPlaying) {
			speedFX.Play();
			isFxPlaying = true;
		}
		if (currentVirtualSpeedNormalized > 0.01f)
		{
			speedFX.transform.localPosition = new Vector3(speedFX.transform.localPosition.x,
														  speedFX.transform.localPosition.y,
														  0.15f * currentVirtualSpeedNormalized);
        }
        else
        {
			speedFX.transform.localPosition = new Vector3(speedFX.transform.localPosition.x,
														  speedFX.transform.localPosition.y,
														  0.1f * currentSpeedNormalized);
		}
	}
}