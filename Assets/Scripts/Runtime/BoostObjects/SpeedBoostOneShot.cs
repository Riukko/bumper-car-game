using TMPro;
using UnityEngine;
using UnityEngine.VFX;

public class SpeedBoostOneShot : BoostObject {
	[SerializeField] private float m_boostValue;
	[SerializeField] private int boostIndex;



	public override void OnBoost(GameObject player) {
		player.GetComponent<PlayerState>().IncreaseBoostGauge(m_boostValue);
        switch (boostIndex)
        {
			case 1:
				player.GetComponent<KartController>().m_sphere.GetComponent<KartCollision>().boost01.Play();
				break;
			case 2:
				player.GetComponent<KartController>().m_sphere.GetComponent<KartCollision>().boost02.Play();
				break;
			case 3:
				player.GetComponent<KartController>().m_sphere.GetComponent<KartCollision>().boost03.Play();
				break;
        }
	}
}