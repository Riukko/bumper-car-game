using UnityEngine;

public abstract class BoostObject : MonoBehaviour {
	public bool hasBeenActivatedThisRound;
	public abstract void OnBoost(GameObject player);
}