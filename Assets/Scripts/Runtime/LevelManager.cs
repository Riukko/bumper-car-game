using System.Collections;

using ClapClapEvent.Scripts.Runtime.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : SingletonBehaviour<LevelManager> {
	public void OnLevel(int levelIndex) {
		StartCoroutine(LoadLevelNextFrame(levelIndex));
	}

	public IEnumerator LoadLevelNextFrame(int levelIndex) {
		yield return null;
		Debug.Log("retour au menu");
		SceneManager.LoadScene(levelIndex);
	}
}