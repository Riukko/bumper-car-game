using System;

using UnityEngine;

using Event = AK.Wwise.Event;

public class PlayerSound : MonoBehaviour {
	public Event m_musicIntro;
	public Event m_musicRaceLoop;
	
	public Event m_ambient;
	
	public Event m_speed;
	
	public Event m_speedRace;

	public Event m_startBurst;
	
	private void Start() {
		m_speed.Post(gameObject);
		m_ambient.Post(gameObject);
	}

	public void OnRestart() {
		m_speed.Post(gameObject);
		m_musicIntro.Post(gameObject);
	}

	public void OnStartRound() {
		m_musicRaceLoop.Post(gameObject);
		m_startBurst.Post(gameObject);
		m_speedRace.Post(gameObject);
	}
}