using ClapClapEvent.Scripts.Runtime.Utils;

using UnityEngine;

public class SlowMotionController : MonoBehaviour {
	[SerializeField] private LayerMask slowMotionLayer;
	[SerializeField] private float slowMotionFactor;
	[SerializeField] private Camera slowMotionCamera;

	[SerializeField] private KartController kartController;
	[SerializeField] private PlayerState m_state;
	private Vector3 baseCameraLocalPos;

	private Quaternion baseCameraRotation;

	private void Start() {
		Time.timeScale = 1;
		baseCameraRotation = slowMotionCamera.transform.rotation;
		m_state = transform.parent.GetComponent<PlayerState>();
	}

	private void OnTriggerEnter(Collider other) {
		if (slowMotionLayer.HaveLayer(other.gameObject.layer) && kartController.currentSpeedNormalized > 0.5) {
			TriggerSlowMotion();
		}
	}

	private void OnTriggerExit(Collider other) {
		if (slowMotionLayer.HaveLayer(other.gameObject.layer)) {
			SlowMotionController otherKart = other.gameObject.GetComponent<SlowMotionController>();
			if (!otherKart.m_state.isDestroyed && !m_state.isDestroyed) {
				slowMotionCamera.depth = -5;
				Time.timeScale = 1;
			}
		}
	}

	public void OnRestart() {
		Time.timeScale = 1;
		slowMotionCamera.depth = -5;
	}

	private void TriggerSlowMotion() {
		Time.timeScale = slowMotionFactor;
		slowMotionCamera.depth = 15;
	}
}