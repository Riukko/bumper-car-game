using Cinemachine;
using ClapClapEvent.Scripts.Runtime.Utils;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerState : MonoBehaviour {
    

    [Header ("Player variable parameters")]
    [SerializeField] public float m_virtualSpeed;
    [SerializeField] public int m_speedMultiplier;
    [SerializeField] public float m_boostGauge;
    [SerializeField] public float m_speedToReach;
    public float m_maxVirtualSpeed;
    public float m_lossPerSeconds;
    public float m_gainPerSeconds;
    public int boostToAddToSpeed;
    public float boostAccumulated;
    public PauseMenuController pauseMenu;
    public bool isDestroyed;
    public SkinnedMeshRenderer playerModel;
    public MeshRenderer playerKartModel;
    public Material player1Material;
    public Material player2Material;
    public Material player1ShipMaterial;
    public Material player2ShipMaterial;
    public GameObject speedFxObject;
    public HUDHandler hudHandler;
    private float timer;

    // Customize by progs
    private Vector3 m_offsetPosition = Vector3.right * 5;
    private LayerMask m_camMask = ~0;
    private int[] m_layersCam = {6, 7};
    private int[] m_speedFxLayersCam = { 13, 14 };
    
    private PlayerInput m_input;
    private KartController m_physicController;
    public CinemachineVirtualCamera m_vcam;
    public Camera m_uiCamera;


    // Getters

    public int PlayerIndex => m_input.user.index;
    
    public float GetSpeed => m_physicController.physicalSpeed + m_virtualSpeed;
    
    public bool gamePaused = false;
    
    private void Start() {

        // Input
        m_input = GetComponent<PlayerInput>();
        
        // Physic
        m_physicController = GetComponent<KartController>();
        
        // Camera
        int layer_cam = m_layersCam[PlayerIndex];
        int layer_speedfx = m_speedFxLayersCam[PlayerIndex];
        int other_layercam = m_layersCam[(PlayerIndex + 1) % 2];
        int other_speedFxlayer = m_speedFxLayersCam[(PlayerIndex + 1) % 2];
        m_camMask = m_camMask.RemoveLayer(other_speedFxlayer);
        m_input.camera.cullingMask = m_camMask.RemoveLayer(other_layercam); ;
        m_vcam = GetComponentInChildren<CinemachineVirtualCamera>();
        m_vcam.gameObject.layer = layer_cam;
        speedFxObject.layer = layer_speedfx;
        Transform kart = transform.Find("Kart");
        m_vcam.m_Follow = kart;
        m_vcam.m_LookAt = kart;


        //Shakes
        CinemachineImpulseListener shake_listener = m_vcam.GetComponent<CinemachineImpulseListener>();
        shake_listener.m_ChannelMask = 1 << (PlayerIndex+1);
        CinemachineImpulseSource shake_caller = m_vcam.GetComponent<CinemachineImpulseSource>();
        shake_caller.m_ImpulseDefinition.m_ImpulseChannel = 1 << (PlayerIndex+1);
        
        // Starting Position init
        transform.position += PlayerIndex == 0 ? m_offsetPosition : -m_offsetPosition;
        transform.rotation = Quaternion.AngleAxis(PlayerIndex == 0 ? 90 : -90, Vector3.up);
    }

    private void Update()
    {
        //Boost UI update
        hudHandler.boostGauge.fillAmount = m_boostGauge / hudHandler.maxUiBoostGauge;

        //Virtual speed UI update
        float clampedRotation = m_virtualSpeed / hudHandler.maxUiVirtualSpeed;
        if(clampedRotation > 0.65f)
        {
            hudHandler.hudAnim.SetBool("isShaking", true);
        }
        else
        {
            hudHandler.hudAnim.SetBool("isShaking", false);
        }
        clampedRotation = (hudHandler.maxDialRotation - hudHandler.minDialRotation) * clampedRotation + hudHandler.minDialRotation;
        hudHandler.dialHand.transform.rotation = Quaternion.Euler(0, 0, clampedRotation);
        
    }

    public void IncreaseVirtualSpeed()
    {
        m_speedToReach += 5f * boostAccumulated / 100f;//Mathf.Min(boostToAddToSpeed, m_boostGauge);
        if(m_speedToReach > m_maxVirtualSpeed)
        {
            m_speedToReach = m_maxVirtualSpeed;
        }
    }

    public void IncreaseBoostGauge(float speedToAdd)
    {
        m_boostGauge += speedToAdd;
    }

    public void DecreaseBoostGauge()
    {
        if (m_boostGauge - 5f * boostAccumulated / 100f >= 0)
        {
            m_boostGauge -= 5f * boostAccumulated / 100f;
        }
        else
        {
            m_boostGauge = 0;
        }
    }

    public void OnRestart() {
        ResetVirtualSpeed();
        ResetBoostGauge();
        hudHandler.hudAnim.SetBool("spamA", false);
        gamePaused = false;
        isDestroyed = false;
    }

    public void ResetVirtualSpeed()
    {
        m_virtualSpeed = 0;
    }

    public void ResetBoostGauge()
    {
        m_boostGauge = 0;
    }



    public void UpdateVirtualSpeedMultiplier(int newMultiplier)
    {
        m_speedMultiplier = newMultiplier;
    }

    public void PauseResume()
    {
        if (pauseMenu.isPaused)
        {
            gamePaused = true;
        }
        else
        {
            gamePaused = false;
        }
    }

    public void SetHudCam()
    {
        hudHandler.SetCanvasCam(m_uiCamera);
    }
}
