using System;

using ClapClapEvent.Scripts.Runtime.CustomEvents;
using ClapClapEvent.Scripts.Runtime.CustomEvents.Imp;
using ClapClapEvent.Scripts.Runtime.Utils;

using UnityEngine;

using Event = AK.Wwise.Event;

public class KartCollision : MonoBehaviour {
	
	[Header("Parts GO")]
	public GameObject visual;
	public Camera slowMoCamera;

	[Header("Music")]
	public Event speedRace;
	public Event speedCollision;
	public Event stopSpeedSFX;
	
	public Event musicRace;
	public Event musicCollision;
	
	public Event boost;
	public Event explosion;

	[Header("Collisions")]
	public GameState gameState;
	public IntGameObjectEvent playerFastest;
	public LayerMask m_playerLayer;
	public LayerMask m_boostItemLayers;
	public LayerMask m_ConfrontationLayer;
	public LayerMask m_wallsLayer;
	
	[Header("Checkpoint")]
	public LayerMask m_checkPointLayer;
	
	[Header("Feedbacks")]
	public ParticleSystem explosionParticles;

	[Header("Boosts")]
	public ParticleSystem boost01;
	public ParticleSystem boost02;
	public ParticleSystem boost03;

	[HideInInspector] public PlayerState m_state;
	private KartController m_kartController;
	private Rigidbody rb;

	private void Start() {
		m_state = transform.parent.GetComponent<PlayerState>();
		m_kartController = transform.parent.GetComponent<KartController>();
		rb = GetComponent<Rigidbody>();
	}

	private void OnCollisionEnter(Collision other) {
		if (m_playerLayer.HaveLayer(other.gameObject.layer))
		{
			musicRace.Post(gameObject);
			KartCollision otherKart = other.gameObject.GetComponent<KartCollision>();
			stopSpeedSFX.Post(gameObject);
			if (Math.Abs(m_state.GetSpeed - otherKart.m_state.GetSpeed) < 0.1f)
			{
				gameState.speeds[m_state.PlayerIndex] = m_state.GetSpeed;
				gameState.speeds[otherKart.m_state.PlayerIndex] = otherKart.m_state.GetSpeed;
				IntGameObject playerFastestEvent = new IntGameObject { };
				playerFastestEvent.first = -1;
				playerFastestEvent.second = gameObject;
				playerFastest.Invoke(playerFastestEvent);
				Explosion();
			}
			else if (m_state.GetSpeed > otherKart.m_state.GetSpeed)
			{
				gameState.speeds[m_state.PlayerIndex] = m_state.GetSpeed;
				gameState.speeds[otherKart.m_state.PlayerIndex] = otherKart.m_state.GetSpeed;
				IntGameObject playerFastestEvent = new IntGameObject { };
				playerFastestEvent.first = m_state.PlayerIndex;
				playerFastestEvent.second = gameObject;
				playerFastest.Invoke(playerFastestEvent);
				speedRace.Post(gameObject);
			}
			else
			{
				Explosion();
			}
		}
		else if (m_wallsLayer.HaveLayer(other.gameObject.layer))
		{
			m_kartController.Steer(
				m_kartController.cross.y < 0 ?
				-1 : 1,
				Mathf.Lerp(10f, 60f, Mathf.InverseLerp(0, 90, m_kartController.angleBetweenVectors)));
		}


	}



    public void Explosion() {
		m_state.isDestroyed = true;
		explosion.Post(gameObject);
		visual.SetActive(false);
		rb.isKinematic = true;
		rb.detectCollisions = false;
		slowMoCamera.enabled = false;
		explosionParticles.Play();
	}
	
	public void OnRestart() {
		//gameState.speeds[m_state.PlayerIndex] = 0;
		visual.SetActive(true);
		rb.isKinematic = false;
		rb.detectCollisions = true;
		slowMoCamera.enabled = true;
	}

	private void OnCollisionExit(Collision other) {
		if (m_playerLayer.HaveLayer(other.gameObject.layer)) { }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (m_boostItemLayers.HaveLayer(other.gameObject.layer))
        { 
            other.GetComponent<BoostObject>().OnBoost(transform.parent.gameObject);
			boost.Post(gameObject);
        }
        else if (m_ConfrontationLayer.HaveLayer(other.gameObject.layer))
        {
			m_state.boostAccumulated = m_state.m_boostGauge;
            m_kartController.currentState = KartController.PlayerStates.CONFRONTATION_ZONE;
            m_kartController.timerSpeedIncrease = 0f;
			m_state.hudHandler.hudAnim.SetBool("spamA", true);
            other.GetComponent<ConfrontationZone>().ActivateStartZone();
			speedCollision.Post(gameObject);
			musicCollision.Post(gameObject);
        }
        if (m_checkPointLayer.HaveLayer(other.gameObject.layer))
        {
            if (m_kartController.currentCheckpoint != other.gameObject)
            {
                m_kartController.currentCheckPointIndex = m_kartController.nextCheckPointIndex;
                m_kartController.nextCheckPointIndex = mod(m_kartController.currentCheckPointIndex + 1, m_kartController.checkPointsList.Count);

                m_kartController.currentCheckpoint =
                    m_state.PlayerIndex == 0 ?
                    m_kartController.checkPointsList[m_kartController.currentCheckPointIndex] :
                    m_kartController.checkPointsList[mod(m_kartController.checkPointsList.Count - m_kartController.currentCheckPointIndex, m_kartController.checkPointsList.Count)];


                m_kartController.nextCheckpoint =
                    m_state.PlayerIndex == 0 ?
                    m_kartController.checkPointsList[m_kartController.nextCheckPointIndex] :
                    m_kartController.checkPointsList[mod(m_kartController.checkPointsList.Count - m_kartController.nextCheckPointIndex, m_kartController.checkPointsList.Count)];
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (m_ConfrontationLayer.HaveLayer(other.gameObject.layer))
        {
            m_kartController.currentState = KartController.PlayerStates.ACCELERATION;
            m_kartController.fixedVirtualSpeed = m_state.m_virtualSpeed;
			m_state.hudHandler.hudAnim.SetBool("spamA", false);
			m_state.m_speedToReach = 0;
            m_kartController.timerSpeedDecrease = 0;
			musicRace.Post(gameObject);
			speedRace.Post(gameObject);
        }
        
    
    }
    public int mod(int x, int m)
    {
        return (x % m + m) % m;
    }
}