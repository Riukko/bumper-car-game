﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class KartController : MonoBehaviour {
    [Header("Main Model")] 
    
    public Transform m_kart;
    public Transform m_kartModel;
    public Transform m_kartNormal;
    public Rigidbody m_sphere;
    public BoxCollider m_slowMoCollider;

    [Header("Parameters")] public float m_maxSpeed = 30f;

    public float m_steering = 80f;
    public float m_driftSteer = 10.0f;
    public float m_gravity = 10f;
    public LayerMask m_groundMask;

    private bool m_drift;
    private Vector2 m_move;
    private float m_rotate, m_currentRotate;
    private float m_speed, m_currentSpeed;
    private bool m_canMove = false;

    //Checkpoint system
    public bool activateCheckpointSystem = false;
    public GameObject currentCheckpoint;
    public GameObject nextCheckpoint;
    public int currentCheckPointIndex;
    public int nextCheckPointIndex;
    public List<GameObject> checkPointsList;
    public bool canSteer = true;
    public Vector3 cross;
    public Vector3 playerForward;
    public Vector3 vectorBetweenCheckpoints;
    public float angleBetweenVectors;

    // Getters
    public float physicalSpeed => m_currentSpeed;
    public float currentSpeedNormalized => Mathf.Clamp01(m_currentSpeed / m_maxSpeed);
    public bool isDrifting => Mathf.Abs(m_move.x) > 0.2f && m_drift && isGrounded && currentSpeedNormalized > 0.4f;
    public bool isGrounded { get; private set; }

    // Kart States
    public enum PlayerStates
    {
        ACCELERATION,
        CONFRONTATION_ZONE
    }
    public PlayerStates currentState = PlayerStates.ACCELERATION;

    [Header("Boost requirements")]
    public ParticleSystem boostParticles;
    public float timerSpeedDecrease;
    public float timerSpeedIncrease;
    public PlayerState m_playerState;
    public float fixedVirtualSpeed;

    public ParticleSystem spawnParticles;

    [Header("Animation")]
    public Animator playerAnimator;
    public AnimationCurve steerAnim;
    public ParticleSystem particleLeft;
    public ParticleSystem particleRight;

    public void OnRestart() {
        // TODO some useless ?
        currentState = PlayerStates.ACCELERATION;
        Debug.Log("Restart Stop");
        spawnParticles.Play();
        m_sphere.velocity = Vector3.zero;
        m_sphere.transform.localPosition = Vector3.zero;
        m_sphere.transform.localRotation = Quaternion.identity;
        m_kart.localRotation = Quaternion.identity;
        m_kartModel.parent.localRotation = Quaternion.identity;
        m_canMove = false;
        m_speed = 0.0f;
        m_currentSpeed = 0.0f;
        m_rotate = 0.0f;
        m_currentRotate = 0.0f;
        m_drift = false;
        ResetCheckpoints();
    }

    public void OnStartRound() {
        if(activateCheckpointSystem)
            currentCheckpoint = checkPointsList[0];
        m_canMove = true;
        m_speed = m_maxSpeed;
    }

    private void Start()
    {
        m_playerState = GetComponent<PlayerState>();
        ResetCheckpoints();
    }

    private void Update() {
        if(currentState == PlayerStates.ACCELERATION)
        {
            if (m_playerState.m_virtualSpeed > 0)
            {
                timerSpeedDecrease += Time.deltaTime;
                m_playerState.m_virtualSpeed = Mathf.Lerp(fixedVirtualSpeed, 0, timerSpeedDecrease / (fixedVirtualSpeed / m_playerState.m_lossPerSeconds ));
            }
        }
        else if(currentState == PlayerStates.CONFRONTATION_ZONE)
        {
            if(m_playerState.m_virtualSpeed != m_playerState.m_speedToReach)
            {
                timerSpeedIncrease += Time.deltaTime;
                m_playerState.m_virtualSpeed = Mathf.Lerp(m_playerState.m_virtualSpeed, m_playerState.m_speedToReach, timerSpeedIncrease / (m_playerState.m_speedToReach / m_playerState.m_gainPerSeconds));
            } 

        }

        if (activateCheckpointSystem)
        {
            UpdatePlayerRoadAngle();
            CheckIfWrongWay();
        }

        //Follow Collider
        m_kart.position = m_sphere.transform.position + new Vector3(0, 0.4f, 0);

        //Follow Collider and slowMo camera
        m_kart.position = m_sphere.transform.position - new Vector3(0, 0.4f, 0);
        m_slowMoCollider.transform.position = m_sphere.transform.position;


        // Steer
        if (m_move != Vector2.zero && canSteer)
        {
            int dir = m_move.x > 0 ? 1 : -1;
            float amount = Mathf.Abs(m_move.x);
            Steer(dir, amount);
            Debug.Log("X : " + amount);
        }


        //Drift and steer flame animations
        if (m_drift)
        {
            if(m_move.x < -0.2f)
            {
                playerAnimator.SetFloat("Blend", steerAnim.Evaluate(m_move.x) - 0.2f);
            }
            else if(m_move.x > 0.2f)
            {
                playerAnimator.SetFloat("Blend", steerAnim.Evaluate(m_move.x) + 0.2f);
            }
            else
            {
                playerAnimator.SetFloat("Blend", 0.5f);
            }
        }
        else
        {
            playerAnimator.SetFloat("Blend", steerAnim.Evaluate(m_move.x));
        }
        UpdateParticles();
            

        //Speed 
        m_currentSpeed = Mathf.SmoothStep(m_currentSpeed, m_speed, Time.deltaTime * 12f);
        m_currentRotate = Mathf.Lerp(m_currentRotate, m_rotate, Time.deltaTime * 4f);
        m_rotate = 0f;

        //Animations    

        //If the game is not paused
        if (!gameObject.GetComponent<PlayerState>().gamePaused)
        {
            //a) Kart
            m_kartModel.parent.localRotation = Quaternion.Lerp(m_kartModel.parent.localRotation,
                Quaternion.AngleAxis( m_move.x * (15.0f + (isDrifting ? 15.0f : 0.0f)),
                    Vector3.up), .2f);
        }
    }

    private void UpdateParticles()
    {
            if(playerAnimator.GetFloat("Blend") < 0.3f)
            {
                if(!particleRight.isPlaying)particleRight.Play();
                particleLeft.Stop(); 
            }
            else if(playerAnimator.GetFloat("Blend") > 0.7f)
            {
                if (!particleLeft.isPlaying) particleLeft.Play();
                particleRight.Stop();
            }
            else { 
                particleLeft.Stop();
                particleRight.Stop();
            }
    }

    private void FixedUpdate() {
        //Gravity
        m_sphere.AddForce(Vector3.down * m_gravity, ForceMode.Acceleration);
        
        RaycastHit hit_near;
        Vector3 position = m_kart.position;
        isGrounded =   Physics.Raycast(position + m_kart.up * .1f, Vector3.down, out hit_near, 2.0f, m_groundMask);

        //Normal Rotation
        m_kartNormal.up = Vector3.Lerp(m_kartNormal.up, hit_near.normal, Time.deltaTime * 8.0f);
        m_kartNormal.Rotate(0, m_kart.eulerAngles.y, 0);
        
        if (!m_canMove) return;
        
        //Forward Acceleration
        m_sphere.AddForce(m_kartModel.transform.forward * m_currentSpeed, ForceMode.Acceleration);
        
        //Steering
        m_kart.eulerAngles = Vector3.Lerp(m_kart.eulerAngles, new Vector3(0, m_kart.eulerAngles.y + m_currentRotate, 0),
            Time.deltaTime * 5f);
    }

    public void Steer(int direction, float amount) {
        float bonus_drift = m_drift && currentSpeedNormalized > 0.2f ? m_driftSteer : 1.0f;
        //if (m_drift && direction < 0)
        //{
        //    playerAnimator.SetFloat("Blend",
        //        playerAnimator.GetFloat("Blend") < 1 ?
        //        Mathf.Lerp(0.5f, 1, Mathf.InverseLerp(0, 0.5f, amount)):
        //        1);
        //}
        //else if (m_drift && direction > 0)
        //{
        //    playerAnimator.SetFloat("Blend",
        //         playerAnimator.GetFloat("Blend") > 0 ?
        //         Mathf.Lerp(0.5f, 0, Mathf.InverseLerp(0, 0.5f, amount)) :
        //         0);
        //}
        //else if(!m_drift && direction < 0)
        //{
        //    playerAnimator.SetFloat("Blend",
        //        playerAnimator.GetFloat("Blend") < 0.6f ?
        //        Mathf.Lerp(0.5f, 0.7f, Mathf.InverseLerp(0, 0.2f, amount)) :
        //        0.6f);
        //}
        //else
        //{
        //    playerAnimator.SetFloat("Blend",
        //            playerAnimator.GetFloat("Blend") > 0.4f ?
        //            Mathf.Lerp(0.5f, 0.3f, Mathf.InverseLerp(0, 0.2f, amount)) :
        //            0.4f);
        //}
        m_rotate = direction * amount * (m_steering + bonus_drift);
    }

    #region Inputs

    public void OnBoost(InputAction.CallbackContext ctx) {
        if (ctx.performed && currentState == PlayerStates.CONFRONTATION_ZONE && m_playerState.m_boostGauge > 0)
        {
            m_playerState.IncreaseVirtualSpeed();
            m_playerState.DecreaseBoostGauge();
            boostParticles.Play();
        }
    }

    public void OnSteer(InputAction.CallbackContext ctx) {
        if (ctx.performed) {
            m_move = ctx.ReadValue<Vector2>();
        } else if (ctx.canceled) {
            m_move = Vector2.zero;
        }
    }

    public void OnDrift(InputAction.CallbackContext ctx) {
        if (ctx.performed) {
            m_drift = true;
        } else if (ctx.canceled) {
            m_drift = false;
        }
    }

    public void OnDeviceLost(PlayerInput input) {
        Debug.Log("Lost");
    }

    public void OnDeviceRegained(PlayerInput input) {
        Debug.Log("Regained");
    }

    public void ResetCheckpoints()
    {
        if (checkPointsList.Count == 0) {
            activateCheckpointSystem = false;
        }
        else
        {
            currentCheckPointIndex = 0;
            nextCheckPointIndex = 1;
            currentCheckpoint = checkPointsList[currentCheckPointIndex];
            nextCheckpoint = m_playerState.PlayerIndex == 0 ? checkPointsList[1] : checkPointsList[checkPointsList.Count - 1];
            
        }
    }

    private void CheckIfWrongWay() {
        if (angleBetweenVectors > 85)
        {
            canSteer = false;
            if (cross.y < 0)
            {
                Steer(-1, 1);
            }
            else
            {
                Steer(1, 1);
            }
            Debug.DrawLine(nextCheckpoint.transform.position, currentCheckpoint.transform.position, Color.red);
            Debug.DrawRay(m_kartModel.transform.position, playerForward, Color.yellow);
        }
        else
        {
            canSteer = true;
        }

    }

    private void UpdatePlayerRoadAngle()
    {
        playerForward = m_kartModel.transform.forward;
        vectorBetweenCheckpoints = nextCheckpoint.transform.position - currentCheckpoint.transform.position;
        playerForward = new Vector3(playerForward.x, 0, playerForward.z);
        vectorBetweenCheckpoints = new Vector3(vectorBetweenCheckpoints.x, 0, vectorBetweenCheckpoints.z);
        angleBetweenVectors = Vector3.Angle(playerForward, vectorBetweenCheckpoints);
        cross = Vector3.Cross(playerForward, vectorBetweenCheckpoints);
    }
    #endregion
}
