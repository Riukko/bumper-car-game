using System;

using ClapClapEvent.Scripts.Runtime.Utils;

using UnityEditor;

using UnityEngine;
using UnityEngine.SceneManagement;

using Event = AK.Wwise.Event;

public class MusicManager : SingletonBehaviour<MusicManager> {
	[SerializeField] private Event m_startMenuMusic;
	[SerializeField] private Event m_stopMenuMusic;
	[SerializeField] private Event m_startMusic;
	[SerializeField] private Event m_stopMusic;

	private void Start() {
		OnLevel(SceneManager.GetActiveScene().buildIndex);
	}

	public void OnLevel(int level) {
		switch (level) {
			case 0:
				m_stopMusic.Post(gameObject);
				m_startMenuMusic.Post(gameObject);
				break;
			case 1:
				m_stopMenuMusic.Post(gameObject);
				m_startMusic.Post(gameObject);
				break;
			default:
				break;
		}
	}
}