using System.Collections;
using System.Collections.Generic;

using ClapClapEvent.Scripts.Runtime.CustomEvents;
using ClapClapEvent.Scripts.Runtime.CustomEvents.Imp;

using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerManager : MonoBehaviour {
	[SerializeField] private PlayerInputEvent m_playerJoinedEvent;
	[SerializeField] private SimpleEvent m_allPlayerJoinedEvent;
	[SerializeField] private GameObject CheckPoints;
	public GameObject hudObject;
	public PauseMenuController pauseController;

	private PlayerInputManager m_manager;
	private HashSet<int> m_playerJoined = new HashSet<int>();
	private readonly List<GameObject> m_players = new List<GameObject>();

	private void Start() {
		m_manager = GetComponent<PlayerInputManager>();
		m_playerJoined = new HashSet<int>();
	}

	public void OnPlayerJoined(PlayerInput input) {
		Debug.Log("Player " + input.user.index + " joined.");
		GameObject g = input.gameObject;
		m_players.Add(g);
		g.name = "Player " + input.user.index;
		g.transform.position = transform.position;
		PlayerState playerState = g.GetComponent<PlayerState>();
		playerState.playerModel.sharedMaterial =
			input.user.index == 0 ? playerState.player1Material : playerState.player2Material;
		playerState.playerKartModel.sharedMaterial =
			input.user.index == 0 ? playerState.player1ShipMaterial : playerState.player2ShipMaterial;
		playerState.hudHandler = input.user.index == 0 ? hudObject.transform.GetChild(0).GetComponent<HUDHandler>()
			: hudObject.transform.GetChild(1).GetComponent<HUDHandler>();
		if(pauseController != null)playerState.pauseMenu = pauseController;
		playerState.SetHudCam();
		if (CheckPoints != null) {
			for (int i = 0; i < CheckPoints.transform.childCount; i++)
			{
				g.GetComponent<KartController>().checkPointsList.Add(CheckPoints.transform.GetChild(i).gameObject);
			}
		}
		StartCoroutine(_RegisterPlayerNewFrame(input));
	}

	private IEnumerator _RegisterPlayerNewFrame(PlayerInput input) {
		yield return null;
		m_playerJoined.Add(input.playerIndex);
		m_playerJoinedEvent.Invoke(input);

		if (m_playerJoined.Count > 1) {
			m_manager.DisableJoining();
			m_allPlayerJoinedEvent.Invoke();
		}
	}

	public void OnPlayerQuit(PlayerInput input) {
		Debug.Log("Player " + input.user.index + " quited.");
		m_playerJoined.Remove(input.user.index);
	}


}