using UnityEngine;

[CreateAssetMenu(fileName = "GameState", menuName = "GameState")]
public class GameState : ScriptableObject {
	public int MaxRound = 5;
	public int[] scores;
	public float[] speeds;

	public void Awake() {
		Reset();
	}

	public void Reset() {
		scores = new[] {0, 0};
		speeds = new[] {0f, 0f };
	}
}