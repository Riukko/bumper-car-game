using System.Collections;

using ClapClapEvent.Scripts.Runtime.CustomEvents;

using TMPro;

using UnityEngine;
using UnityEngine.InputSystem;

using Event = AK.Wwise.Event;

public class WaitPlayers : MonoBehaviour {
	public Event m_soundCount;
	public Event m_soundCountLast;
	
	[SerializeField] private SimpleEvent m_startRound;
	[SerializeField] private GameState gameState;
	public TextMeshProUGUI m_text;
	public GameObject UiRules;
	public TextMeshProUGUI m_textRules;

	private Camera m_cam;
	private Canvas m_canvas;

	private void Start() {
		m_cam = GetComponent<Camera>();
		m_canvas = GetComponentInChildren<Canvas>();
		UiRules.SetActive(true);
		m_text.enabled = false;
	}

	private void OnPlayerJoined(PlayerInput player) {
		m_textRules.text = "Player " + (player.playerIndex + 1) + " joined. Waiting player 2...";
	}

	private void OnAllPlayerJoined() {
		m_text.enabled = true;
		UiRules.SetActive(false);
		gameState.scores[0] = 0;
		gameState.scores[1] = 0;
		m_cam.enabled = false;
		StartCoroutine(_StartRound());
	}

	public void OnRestart() {
		m_canvas.enabled = true;
		StartCoroutine(_StartRound());
	}

	public IEnumerator _StartRound() {
		int i = 3;
		while (i > 0) {
			Debug.Log(i);
			m_text.text = i.ToString();
			m_soundCount.Post(gameObject);
			yield return new WaitForSeconds(1);
			i--;
		}
		m_soundCountLast.Post(gameObject);
		m_startRound.Invoke();
		m_text.text = "Go";
		yield return new WaitForSeconds(2);
		m_canvas.enabled = false;
	}
}