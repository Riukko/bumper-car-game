using UnityEngine;

public class ConfrontationZone : MonoBehaviour
{
    [SerializeField] GameObject confrontationZoneStart;

	public void ActivateStartZone() {
		if (confrontationZoneStart != null) {
			confrontationZoneStart.SetActive(true);
		}
	}

	public void OnRestart() {
		if (confrontationZoneStart != null) {
			confrontationZoneStart.SetActive(false);
		}
	}
}