using System.Collections;

using ClapClapEvent.Scripts.Runtime.CustomEvents;
using ClapClapEvent.Scripts.Runtime.CustomEvents.Imp;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	[SerializeField] private GameState m_gameState;
	[SerializeField] private SimpleEvent m_restartEvent;
	[SerializeField] private IntEvent m_loadLevelEvent;

	[SerializeField] private Canvas m_winPanel;
	[SerializeField] private GameObject m_matchWinPanel;
	[SerializeField] private GameObject m_roundWinPanel;
	[SerializeField] private TextMeshProUGUI m_text;
	[SerializeField] private Image roundWinImage;
	[SerializeField] private Image matchWinImage;


	[SerializeField] private Sprite p1RoundVictory;
	[SerializeField] private Sprite p2RoundVictory;
	[SerializeField] private Sprite p1MatchVictory;
	[SerializeField] private Sprite p2MatchVictory;
	[SerializeField] private Sprite roundDraw;


	private bool m_isRoundFinish;

	private void Start() {
		m_isRoundFinish = false;
	}

	public void OnFastestPlayer(IntGameObject fastestPlayer) {
		if (!m_isRoundFinish) {
			m_isRoundFinish = true;
			if (fastestPlayer.first < 0) {
				roundWinImage.sprite = roundDraw;
				m_roundWinPanel.SetActive(true);
				m_matchWinPanel.SetActive(false);
				m_text.text = "SCORE " + m_gameState.scores[0] + " - " + m_gameState.scores[1] + "\n\n";
				m_text.text += "VITESSE : " + (int)m_gameState.speeds[0];
				m_winPanel.enabled = true;
				StartCoroutine(EndLevel(fastestPlayer.first));
				return;
			}
			m_gameState.scores[fastestPlayer.first]++;
			roundWinImage.sprite = fastestPlayer.first == 0 ? p1RoundVictory : p2RoundVictory;
			m_roundWinPanel.SetActive(true);
			m_matchWinPanel.SetActive(false);
			m_text.text = "SCORE " + m_gameState.scores[0] + " - " + m_gameState.scores[1] + "\n\n";
			m_text.text += "VITESSES :" + (int)m_gameState.speeds[0] + " - " + (int)m_gameState.speeds[1];
			m_winPanel.enabled = true;
			StartCoroutine(EndLevel(fastestPlayer.first));
		}
	}

	public void OnMatchDraw() {
		if (!m_isRoundFinish) {
			m_isRoundFinish = true;
			m_winPanel.enabled = true;
			m_text.text = "Score " + m_gameState.scores[0] + " - " + m_gameState.scores[1];
			StartCoroutine(EndLevelDraw());
		}
	}

	private IEnumerator EndLevel(int indexPlayer) {
		if(indexPlayer >= 0 && m_gameState.scores[indexPlayer] > m_gameState.MaxRound / 2){
			matchWinImage.sprite = indexPlayer == 0 ? p1MatchVictory : p2MatchVictory;
			m_roundWinPanel.SetActive(false);
			m_matchWinPanel.SetActive(true);
			m_winPanel.enabled = true;
        }
		yield return new WaitForSecondsRealtime(5.0f);
		if (indexPlayer >= 0 && m_gameState.scores[indexPlayer] > m_gameState.MaxRound / 2) {
			m_loadLevelEvent.Invoke(0);
		} else {
			Debug.Log("Restart");
			m_restartEvent.Invoke();
			m_isRoundFinish = false;
			m_winPanel.enabled = false;
		}
	}

	private IEnumerator EndLevelDraw() {
		yield return new WaitForSecondsRealtime(5.0f);
		m_loadLevelEvent.Invoke(1);
	}
}