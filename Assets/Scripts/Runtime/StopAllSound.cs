using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

using Event = AK.Wwise.Event;

public class StopAllSound : MonoBehaviour {
    [SerializeField] private Event m_stopSpeed;
    [SerializeField] private Event m_stopAmb;
    
    private void Start() {
        m_stopSpeed.Post(gameObject);
        m_stopAmb.Post(gameObject);
    }
}
