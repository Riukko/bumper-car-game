using System;
using System.Collections;
using System.Collections.Generic;
using ClapClapEvent.Scripts.Runtime.CustomEvents;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class PausePlayerController : MonoBehaviour {
    [SerializeField] SimpleEvent m_pauseInputPressed;

    private void OnPause(InputAction.CallbackContext ctx) {
        if (ctx.performed) {
            m_pauseInputPressed.Invoke();
        }
    }
}
