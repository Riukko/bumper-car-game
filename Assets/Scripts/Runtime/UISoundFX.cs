using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Event = AK.Wwise.Event;

public class UISoundFX : MonoBehaviour
{
   [SerializeField] private Event SFXUIClick;
   [SerializeField] private Event SFXUIBack;
   [SerializeField] private Event SFXUIHoover;
   
   public void OnSFXUIClick() {
      SFXUIClick.Post(gameObject);
   }
   
   public void OnSFXUIBack() {
      SFXUIBack.Post(gameObject);
   }
   
   public void OnSFXUIHover() {
      SFXUIHoover.Post(gameObject);
   }
}
