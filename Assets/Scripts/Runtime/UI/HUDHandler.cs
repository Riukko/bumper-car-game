using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDHandler : MonoBehaviour
{
    public Camera playerUICamera;
    public Image boostGauge;
    public Image dialHand;
    public float maxDialRotation;
    public float minDialRotation;
    public float maxUiBoostGauge;
    public float maxUiVirtualSpeed;
    public Animator hudAnim;

    public void SetCanvasCam(Camera cam)
    {
        GetComponent<Canvas>().worldCamera = cam;
        GetComponent<Canvas>().enabled = true;
    }

    
}
