using System.Collections.Generic;

using ClapClapEvent.Scripts.Runtime.CustomEvents.Imp;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour {
	[SerializeField] private GameObject pauseCanvas;
	[SerializeField] private GameObject pauseMenu;
	[SerializeField] private GameObject ResumeButton;
	[SerializeField] private GameObject promptQuitMainMenu;
	[SerializeField] private GameObject promptQuitGame;
	[SerializeField] private GameObject yesMainMenu;
	[SerializeField] private GameObject yesQuit;
	[SerializeField] private GameObject resumeButton;

	[SerializeField] private List<MultiplayerEventSystem> uievents;

	public bool isPaused;
	public bool allPlayerJoined;

	public BoolEvent pauseEvent;
	public IntEvent levelEvent;

	public void OnPlayerJoined(PlayerInput player) {
		MultiplayerEventSystem ev = player.GetComponentInChildren<MultiplayerEventSystem>();
		uievents.Add(ev);
		ev.firstSelectedGameObject = resumeButton;
	}

	public void OnPauseInputPressed() {
		if (!isPaused && allPlayerJoined) {
			PauseGame();
			promptQuitGame.SetActive(false);
			promptQuitGame.SetActive(false);
		} else if (isPaused && allPlayerJoined) {
			ResumeGame();
		}
		promptQuitGame.SetActive(false);
		promptQuitMainMenu.SetActive(false);
	}

	public void OnAllPlayerJoined() {
		allPlayerJoined = true;
	}

	public void PauseGame() {
		isPaused = true;
		Time.timeScale = 0;
		pauseCanvas.GetComponent<Canvas>().enabled = true;
		pauseEvent.Invoke(true);
		foreach (MultiplayerEventSystem ev in uievents) {
			ev.SetSelectedGameObject(ResumeButton);
		}
	}

	public void ResumeGame() {
		isPaused = false;
		Time.timeScale = 1;
		pauseEvent.Invoke(true);
		pauseCanvas.GetComponent<Canvas>().enabled = false;
		foreach (MultiplayerEventSystem ev in uievents) {
			ev.SetSelectedGameObject(ResumeButton);
		}
	}

	public void MainMenuButton() {
		promptQuitMainMenu.SetActive(true);
		foreach (MultiplayerEventSystem ev in uievents) {
			ev.SetSelectedGameObject(yesMainMenu);
		}
	}

	public void QuitGameButton() {
		promptQuitGame.SetActive(true);
		foreach (MultiplayerEventSystem ev in uievents) {
			ev.SetSelectedGameObject(yesQuit);
		}
	}

	public void QuitToMainMenu()
    {
        if (isPaused)
        {
			levelEvent.Invoke(0);
        }
    }

	public void CancelPrompt() {
		promptQuitGame.SetActive(false);
		promptQuitMainMenu.SetActive(false);
		foreach (MultiplayerEventSystem ev in uievents) {
			ev.SetSelectedGameObject(resumeButton);
		}
	}

	public void QuitGame() {
		if(isPaused)Application.Quit();
	}
}