using ClapClapEvent.Scripts.Runtime.CustomEvents.Imp;

using UnityEngine;
using UnityEngine.InputSystem.UI;

public class MainMenuController : MonoBehaviour {
	[SerializeField] private IntEvent levelEvent;
	[SerializeField] private GameObject StartButton;
	[SerializeField] private GameObject creditsMenu;
	[SerializeField] private GameObject creditsBackButton;
	[SerializeField] private GameObject controlsMenu;
	[SerializeField] private GameObject controlsBackButton;

	[SerializeField] private MultiplayerEventSystem[] events;

	public void OnBackCredits() {
		creditsMenu.SetActive(false);
		foreach (MultiplayerEventSystem ev in events) {
			ev.SetSelectedGameObject(StartButton);
		}
	}
	
	

	public void OnBackControls() {
		controlsMenu.SetActive(false);
		foreach (MultiplayerEventSystem ev in events) {
			ev.SetSelectedGameObject(StartButton);
		}
	}

	public void OnCreditsButton() {
		creditsMenu.SetActive(true);
		foreach (MultiplayerEventSystem ev in events) {
			ev.SetSelectedGameObject(creditsBackButton);
		}
	}
	
	public void OnControlsButton() {
		controlsMenu.SetActive(true);
		foreach (MultiplayerEventSystem ev in events) {
			ev.SetSelectedGameObject(controlsBackButton);
		}
	}

	public void QuitGame() {
		Application.Quit();
	}
}