using System.Linq;

using ClapClapEvent.Scripts.Runtime.CustomEvents;

using UnityEngine;

[ExecuteAlways]
//[CreateAssetMenu(fileName = "DebugEvent", menuName = "Events/Debug")]
public class DebugEventValue : ScriptableObject {
	[SerializeField] private DebugLevel m_valueDebugLevel = DebugLevel.Warning;

	[SerializeField] private Color[] m_colorsDebug = {Color.yellow, Color.blue, Color.gray};

	public DebugLevel debugValue {
		get => m_valueDebugLevel;
		set {
			m_valueDebugLevel = value;
			Globals.debugLevel = m_valueDebugLevel;
		}
	}

	public Color[] debugColors {
		get => m_colorsDebug;
		set {
			m_colorsDebug = value;
			Globals.colorsDebug = m_colorsDebug;
		}
	}
	
	private void Awake() {
		Init();
	}

	private void OnEnable() {
		Init();
	}
	
	private void Init() {
		Globals.debugLevel = m_valueDebugLevel;
		Globals.colorsDebug = m_colorsDebug;
	}

}