namespace ClapClapEvent.Scripts.Runtime.CustomEvents {
	public delegate void EventCallback<T>(T param);

	/// <summary>
	///     ParametrisedEvent permit to trigger callbacks with parameters.
	///     Need to be inherited in order to have serialisation in Unity.
	/// </summary>
	/// <typeparam name="T">The type of the parameter.</typeparam>
	public abstract class ParametrisedEvent<T> : EventClapClap {
		private EventCallback<T> m_call;

		public void AddListener(EventCallback<T> listener) {
			m_call += listener;
		}

		public void Invoke(T param) {
			if (m_call != null) {
				m_call(param);
				Globals.DebugLog("Event " + name + " called. (" + param + ")", this);
			} else {
				Globals.DebugLogWarning("No event registered on " + name, this);
			}
		}

		public void RemoveListener(EventCallback<T> listener) {
			m_call -= listener;
		}

		public override string GetEventName() {
			return name.Replace("Event", "");
		}
	}
}