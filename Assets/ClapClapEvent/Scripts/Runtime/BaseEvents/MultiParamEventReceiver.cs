namespace ClapClapEvent.Scripts.Runtime.CustomEvents {
	using System;
	using System.Collections.Generic;

	using UnityEngine;
	using UnityEngine.Events;

	/// <summary>
	///     Listener that permit to bind an ParametrisedEvent to local Unity Events.
	/// </summary>
	[Serializable]
	public class ParamEventListener<TEvent, TParam> : ISerializationCallbackReceiver
		where TEvent : ParametrisedEvent<TParam> {
		#region NameCosmetic
		[SerializeField, HideInInspector] private string m_name;
		#endregion

		[SerializeField] public TEvent m_ev;

		[SerializeField] public UnityEvent<TParam> m_func;

		#region _NameCosmeticMethods
		void ISerializationCallbackReceiver.OnBeforeSerialize() {
			m_name = m_ev != null ? m_ev.GetEventName() : "Null";
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize() { }
		#endregion
	}

	/// <summary>
	///     Receiver of ParametrisedEvent.
	///     Need to be inherited in order to have serialisation in Unity.
	/// </summary>
	/// <typeparam name="TParam">The type of the parameter corresponding of callback (must be the same as ParametrisedEvent).</typeparam>
	[DisallowMultipleComponent]
	public abstract class MultiParamEventReceiver<TParam> : EventReceiver {
		[SerializeField]
		private List<ParamEventListener<ParametrisedEvent<TParam>, TParam>> m_listeners =
			new List<ParamEventListener<ParametrisedEvent<TParam>, TParam>>();

		public void OnEnable() {
			foreach (ParamEventListener<ParametrisedEvent<TParam>, TParam> listener in m_listeners) {
				ParametrisedEvent<TParam> ev = listener.m_ev;
				if (ev != null) {
					ev.AddListener(listener.m_func.Invoke);
					Globals.DebugLog("Event " + ev.name + " listen by " + name, this);
				} else {
					Globals.DebugLogWarning("No event on " + GetType() + "(" + gameObject + ")", this);
				}
			}
		}

		public void OnDisable() {
			foreach (ParamEventListener<ParametrisedEvent<TParam>, TParam> listener in m_listeners) {
				ParametrisedEvent<TParam> ev = listener.m_ev;
				if (ev != null) {
					ev.RemoveListener(listener.m_func.Invoke);
					Globals.DebugLog("Event " + ev.name + " unsubscribed by " + name, this);
				}
			}
		}

#if UNITY_EDITOR
		public void AddPersistentEventHandler(ParametrisedEvent<TParam> ev) {
			ParamEventListener<ParametrisedEvent<TParam>, TParam> listener =
				new ParamEventListener<ParametrisedEvent<TParam>, TParam> {
					m_ev = ev
				};
			m_listeners.Add(listener);
		}
#endif
	}
}