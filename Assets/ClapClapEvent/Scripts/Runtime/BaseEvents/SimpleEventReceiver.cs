﻿namespace ClapClapEvent.Scripts.Runtime.CustomEvents {
	using System;
	using System.Collections.Generic;

	using UnityEngine;
	using UnityEngine.Events;

	/// <summary>
	///     Listener that permit to bind an SimpleEvent to local Unity Events.
	/// </summary>
	[Serializable]
	public struct EventListener : ISerializationCallbackReceiver {
		#region _NameCosmetic
		[SerializeField, HideInInspector] private string m_name;
		#endregion

		[SerializeField] public SimpleEvent m_ev;

		[SerializeField] public UnityEvent m_func;

		#region _NameCosmeticMethods
		void ISerializationCallbackReceiver.OnBeforeSerialize() {
			m_name = m_ev != null ? m_ev.GetEventName() : "Null";
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize() { }
		#endregion
	}

	/// <summary>
	///     Receiver of SimpleEvent.
	/// </summary>
	[DisallowMultipleComponent]
	public class SimpleEventReceiver : EventReceiver {
		[SerializeField] private List<EventListener> m_listeners = new List<EventListener>();

		public void OnEnable() {
			foreach (EventListener listener in m_listeners) {
				SimpleEvent ev = listener.m_ev;
				if (ev != null) {
					ev.AddListener(listener.m_func.Invoke);
					Globals.DebugLog("Event " + ev.name + " listen by " + name, gameObject);
				} else {
					Globals.DebugLogWarning("No event on " + GetType() + "(" + gameObject + ")", this);
				}
			}
		}

		public void OnDisable() {
			foreach (EventListener listener in m_listeners) {
				SimpleEvent ev = listener.m_ev;
				if (ev != null) {
					ev.RemoveListener(listener.m_func.Invoke);
					Globals.DebugLog("Event " + ev.name + " unsubscribed by " + name, this);
				}
			}
		}
#if UNITY_EDITOR
		public void AddPersistentEventHandler(SimpleEvent ev) {
			EventListener listener = new EventListener {
				m_ev = ev
			};
			m_listeners.Add(listener);
		}
#endif
	}
}