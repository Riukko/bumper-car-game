namespace ClapClapEvent.Scripts.Runtime.CustomEvents {
	using UnityEngine;

	public enum DebugLevel {
		None = -2,
		Error = -1,
		Warning = 0,
		Verbose = 1
	}

	public static class Globals {
		public static DebugLevel debugLevel = DebugLevel.Warning;

		public static Color[] colorsDebug = {Color.yellow, Color.blue, Color.gray};

		public static void DebugLog(object message, Object context) {
			if (debugLevel < DebugLevel.Verbose) {
				return;
			}
			switch (context) {
				case EventClapClap _:
					Debug.Log("<color=#" + ColorUtility.ToHtmlStringRGB(colorsDebug[0]) + ">" + message + "</color>", context);

					break;
				case EventReceiver _:
					Debug.Log("<color=#" + ColorUtility.ToHtmlStringRGB(colorsDebug[1]) + ">" + message + "</color>", context);

					break;
				default:
					Debug.Log("<color=#" +ColorUtility.ToHtmlStringRGB( colorsDebug[2]) + ">" + message + "</color>", context);

					break;
			}
		}

		public static void DebugLogWarning(object message, Object context) {
			if (debugLevel < DebugLevel.Warning) {
				return;
			}
			switch (context) {
				case EventClapClap _:
					Debug.LogWarning("<color=#" + ColorUtility.ToHtmlStringRGB(colorsDebug[0]) + ">" + message + "</color>", context);

					break;
				case EventReceiver _:
					Debug.LogWarning("<color=#" + ColorUtility.ToHtmlStringRGB(colorsDebug[1] )+ ">" + message + "</color>", context);

					break;
				default:
					Debug.LogWarning("<color=#" + ColorUtility.ToHtmlStringRGB(colorsDebug[2]) + ">" + message + "</color>", context);

					break;
			}
		}

		public static void DebugLogError(object message, Object context) {
			if (debugLevel < DebugLevel.Error) {
				return;
			}
			switch (context) {
				case EventClapClap _:
					Debug.LogError("<color=#" + ColorUtility.ToHtmlStringRGB(colorsDebug[0]) + ">" + message + "</color>", context);

					break;
				case EventReceiver _:
					Debug.LogError("<color=#" + ColorUtility.ToHtmlStringRGB(colorsDebug[1]) + ">" + message + "</color>", context);

					break;
				default:
					Debug.LogError("<color=#" + ColorUtility.ToHtmlStringRGB(colorsDebug[2]) + ">" + message + "</color>", context);

					break;
			}
		}
	}

	/// <summary>
	///     Base class of all events. Permit to make search and reflection for editor.
	/// </summary>
	public abstract class EventClapClap : ScriptableObject {
		public abstract string GetEventName();
	}

	/// <summary>
	///     Base class of all receiver. Permit to make search and reflection for editor.
	///     Receiver is the container of multiple listeners bind to an event.
	/// </summary>
	public abstract class EventReceiver : MonoBehaviour { }
}