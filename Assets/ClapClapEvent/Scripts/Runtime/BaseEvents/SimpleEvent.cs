﻿namespace ClapClapEvent.Scripts.Runtime.CustomEvents {
	using UnityEngine;

	public delegate void SimpleEventCallback();

	/// <summary>
	///     SimpleEvent permit to trigger void callbacks.
	/// </summary>
	[CreateAssetMenu(fileName = "SimpleEvent", menuName = "Events/SimpleEvent")]
	public class SimpleEvent : EventClapClap {
		private SimpleEventCallback m_call;

		public void AddListener(SimpleEventCallback listener) {
			m_call += listener;
		}

		public void Invoke() {
			if (m_call != null) {
				m_call();
				Globals.DebugLog("Event " + name + " called.", this);
			} else {
				Globals.DebugLogWarning("No event registered on " + name, this);
			}
		}

		public void RemoveListener(SimpleEventCallback listener) {
			m_call -= listener;
		}

		public override string GetEventName() {
			return name.Replace("Event", "");
		}
	}
}