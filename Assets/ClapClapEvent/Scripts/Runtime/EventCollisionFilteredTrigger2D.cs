namespace ClapClapEvent.Scripts.Runtime {
	using UnityEngine;
	using UnityEngine.Events;

	using Utils;

	[RequireComponent(typeof(Collider2D))]
	public class EventCollisionFilteredTrigger2D : MonoBehaviour {
		#region Inspector
#pragma warning disable 0649

		[SerializeField] private LayerMask m_layersDetected;

#pragma warning restore 0649
		#endregion

		private void Start() {
			GetComponent<Collider>().isTrigger = true;
		}

		private void OnTriggerEnter2D(Collider2D other) {
			if (m_layersDetected.HaveLayer(other.gameObject.layer)) {
				m_onTriggerFilteredEnter.Invoke(other.gameObject);
			}
		}

		private void OnTriggerExit2D(Collider2D other) {
			if (m_layersDetected.HaveLayer(other.gameObject.layer)) {
				m_onTriggerFilteredExit.Invoke(other.gameObject);
			}
		}

		private void OnTriggerStay2D(Collider2D other) {
			if (m_layersDetected.HaveLayer(other.gameObject.layer)) {
				m_onTriggerFilteredStay.Invoke(other.gameObject);
			}
		}

		#region LocalEvents
#pragma warning disable 0649
		[Header("Events"), SerializeField] private UnityEvent<GameObject> m_onTriggerFilteredEnter;

		[SerializeField] private UnityEvent<GameObject> m_onTriggerFilteredStay;

		[SerializeField] private UnityEvent<GameObject> m_onTriggerFilteredExit;

		public UnityEvent<GameObject> triggerFilteredEnter => m_onTriggerFilteredEnter;
		public UnityEvent<GameObject> triggerFilteredStay  => m_onTriggerFilteredStay;
		public UnityEvent<GameObject> triggerFilteredExit  => m_onTriggerFilteredExit;

#pragma warning restore 0649
		#endregion
	}
}