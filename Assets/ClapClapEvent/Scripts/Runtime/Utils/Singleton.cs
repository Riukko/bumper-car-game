﻿namespace ClapClapEvent.Scripts.Runtime.Utils {
	/// <summary>
	///     Base class for singleton.
	/// </summary>
	/// <typeparam name="T"> The class to apply the singleton pattern.</typeparam>
	public class Singleton<T> where T : class, new() {
		private static T m_Instance;

		public T instance {
			get {
				if (m_Instance == null) {
					m_Instance = new T();
				}

				return m_Instance;
			}
		}
	}
}