namespace ClapClapEvent.Scripts.Editor.Utils {
	using System;
	using System.Collections.Generic;
	using System.Text.RegularExpressions;

	using Runtime.Utils;

	using UnityEditor;

	public static class SerializedPropertyExtentions {
		private static readonly Regex arrayIndexCapturePattern = new Regex(@"\[(\d*)\]");

		/// <summary>
		///     Get the field from a SerializedProperty.
		///     Don't handle array fields.
		/// </summary>
		/// <param name="prop">SerializedProperty</param>
		/// <typeparam name="T">Type of the field.</typeparam>
		/// <returns>The first field contained in property.</returns>
		public static T GetTargetField<T>(this SerializedProperty prop) {
			string[] property_names = prop.propertyPath.Split('.');
			object target = prop.serializedObject.targetObject;
			bool is_next_property_array_index = false;
			object result = null;
			for (int i = 0; i < property_names.Length && target != null; ++i) {
				string prop_name = property_names[i];
				if (prop_name == "Array") {
					is_next_property_array_index = true;
				} else if (is_next_property_array_index) {
					is_next_property_array_index = false;
					int array_index = ParseArrayIndex(prop_name);
					// ReSharper disable once PossibleInvalidCastException
					object[] target_as_array = (object[]) target;
					result = target_as_array[array_index];
				} else {
					result = ReflectionUtilities.GetField(target, prop_name);
				}
			}

			return (T) result;
		}

		/// <summary>
		///     Get a list corresponding to a field from a SerializedProperty.
		/// </summary>
		/// <param name="prop">SerializedProperty</param>
		/// <typeparam name="T">Type of the field.</typeparam>
		/// <returns>The list of all objects contained in field.</returns>
		public static List<T> GetTargetsField<T>(this SerializedProperty prop) {
			List<T> result = new List<T>();
			string[] property_names = prop.propertyPath.Split('.');
			object[] targets = prop.serializedObject.targetObjects;
			foreach (object obj in targets) {
				object target = obj;
				bool is_next_property_array_index = false;
				for (int i = 0; i < property_names.Length && target != null; ++i) {
					string prop_name = property_names[i];
					if (prop_name == "Array") {
						is_next_property_array_index = true;
					} else if (is_next_property_array_index) {
						is_next_property_array_index = false;
						int array_index = ParseArrayIndex(prop_name);
						// ReSharper disable once PossibleInvalidCastException
						object[] target_as_array = (object[]) target;
						target = target_as_array[array_index];
					} else {
						target = ReflectionUtilities.GetField(target, prop_name);
					}
				}
				result.Add((T) target);
			}

			return result;
		}

		private static int ParseArrayIndex(string propName) {
			Match match = arrayIndexCapturePattern.Match(propName);
			if (!match.Success) {
				throw new Exception($"Invalid array index parsing in {propName}");
			}

			return int.Parse(match.Groups[1].Value);
		}
	}
}