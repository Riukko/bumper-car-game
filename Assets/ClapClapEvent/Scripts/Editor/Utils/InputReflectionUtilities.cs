namespace ClapClapEvent.Scripts.Runtime.Utils {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using System.Text;

	using UnityEditor;
	using UnityEditor.Events;

	using UnityEngine;
	using UnityEngine.Events;
	using UnityEngine.InputSystem;

	public static class InputUnityEventExtension {
		/// <summary>
		///     Search and register events for UnityInput script.
		///     Use it in context menu.
		/// </summary>
		/// <param name="menuCommand">Argument for context menu.</param>
		[MenuItem("CONTEXT/PlayerInput/Auto-reference events reflection")]
		public static void AutoReferenceUnityInput(MenuCommand menuCommand) {
			PlayerInput playerinput = menuCommand.context as PlayerInput;

			//Search functions with CallbackContext as arguments (used only in input event).
			List<MethodInfo> method_infos = playerinput.gameObject.GetMethods(typeof(void),
																			  new[]
																				  {typeof(InputAction.CallbackContext)},
																			  ReflectionUtilities.bindFlags,
																			  new[] {playerinput});

			//Filter and register foreach input action the corresponding methods
			foreach (InputActionMap actionmap in playerinput.actions.actionMaps) {
				foreach (InputAction action in actionmap.actions) {
					List<MethodInfo> methods = method_infos.Where(x => x.Name == "On" + action.name).ToList();

					foreach (MethodInfo info in methods) {
						UnityAction<InputAction.CallbackContext> method_delegate =
							Delegate.CreateDelegate(typeof(UnityAction<InputAction.CallbackContext>),
													playerinput.gameObject.GetComponent(info.DeclaringType), info) as
								UnityAction<InputAction.CallbackContext>;
						PlayerInput.ActionEvent playeraction =
							playerinput.actionEvents.First(x => x.actionName.Contains(action.name));
						UnityEventTools.RemovePersistentListener(playeraction, method_delegate);
						UnityEventTools.AddPersistentListener(playeraction, method_delegate);
					}
				}
			}

			EditorUtility.SetDirty(playerinput);

			//Debug a message report
			StringBuilder sb = new StringBuilder();
			sb.Append("Registration events from " + playerinput.gameObject + ".").Append("\n");
			foreach (PlayerInput.ActionEvent playeraction in playerinput.actionEvents) {
				for (int i = 0; i < playeraction.GetPersistentEventCount(); i++) {
					sb.Append(playeraction.GetPersistentTarget(i).GetType() + "/" +
							  playeraction.GetPersistentMethodName(i) + " function registred.").Append("\n");
				}
			}
			Debug.Log(sb.ToString());
		}
	}
}