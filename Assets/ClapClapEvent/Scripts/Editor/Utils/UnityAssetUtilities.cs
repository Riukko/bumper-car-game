namespace ClapClapEvent.Scripts.Editor.Utils {
	using System.Collections.Generic;
	using System.IO;

	using UnityEditor;

	using UnityEngine;

	public static class UnityAssetUtilities {
		public static string GetActualPathMenuItem() {
			string path = AssetDatabase.GetAssetPath(Selection.activeObject);
			if (path == "") {
				path = "Assets";
			} else if (Path.GetExtension(path) != "") {
				path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
			}

			return path;
		}

		[MenuItem("Assets/DisplayAssetsInfo")]
		public static void DisplayAssetsInfos(MenuCommand command) {
			foreach (string guid in Selection.assetGUIDs) {
				string asset_path = AssetDatabase.GUIDToAssetPath(guid);
				long file;
				Object obj = AssetDatabase.LoadMainAssetAtPath(asset_path);
				if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(obj, out _, out file)) {
					Debug.Log("Asset: " + obj.name + "\n"
							  + "Path: " + asset_path + "\n"
							  + "Type: " + obj.GetType() + "\n"
							  + "Instance ID: " + obj.GetInstanceID() + "\n"
							  + "GUID: " + guid + "\n"
							  + "File ID: " + file);
				}
			}
		}

		//Based on : https://answers.unity.com/questions/321615/code-to-mimic-find-references-in-scene.html
		/// <summary>
		///     Get a list of Monobehaviour that reference a ScriptableObject.
		/// </summary>
		/// <param name="to">ScriptableObject</param>
		/// <returns>A List of MonoBehaviour.</returns>
		public static List<MonoBehaviour> GetListSoReferencesInScene(ScriptableObject to) {
			List<MonoBehaviour> referenced_by = new List<MonoBehaviour>();
			string to_name = string.Format("{0}.{1}", to.name, to.GetType().Name);
			foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
			{
				Component[] components = go.GetComponents<Component>();
				for (int i = 0; i < components.Length; i++) {
					Component component = components[i];
					if (!component) {
						continue;
					}
					SerializedObject so = new SerializedObject(component);
					SerializedProperty sp = so.GetIterator();
					while (sp.NextVisible(true)) {
						if (sp.propertyType == SerializedPropertyType.ObjectReference) {
							if (sp.objectReferenceValue == to) {
								if (component is MonoBehaviour behaviour) {
									referenced_by.Add(behaviour);
								}
							}
						}
					}
				}
			}

			return referenced_by;
		}
	}
}
