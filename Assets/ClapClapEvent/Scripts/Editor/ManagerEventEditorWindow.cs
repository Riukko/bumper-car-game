using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace ClapClapEvent.Scripts.Editor.CustomEvents {
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Reflection;

	using Runtime.CustomEvents;
	using Runtime.Utils;

	using UnityEditor;

	using UnityEngine;

	using Utils;

	using Object = UnityEngine.Object;

	/// <summary>
	///     Permit to visualize all informations about an event.
	/// </summary>
	[Serializable]
	public class EventVisualiser : ISerializationCallbackReceiver {
		#region _NameCosmetic
		[SerializeField, HideInInspector] private string m_name;
		#endregion

		[SerializeField] public EventClapClap m_ev;
		[SerializeField] public MonoBehaviour[] m_listeners;
		[SerializeField] public MonoBehaviour[] m_callers;

		#region _NameCosmeticMethods
		void ISerializationCallbackReceiver.OnBeforeSerialize() {
			m_name = m_ev != null ? m_ev.GetEventName() : "Null";
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize() { }
		#endregion
	}

	/// <summary>
	///     Manger editor that permit to simplify the manipulation of events and recievers.
	/// </summary>
	public class ManagerEventEditorWindow : EditorWindow {
		[SerializeField] private EventVisualiser[] m_referencedInScene;
		[SerializeField] private EventClapClap[] m_notUsedInScene;
		[SerializeField] private EventClapClap m_selected;

		[SerializeField] private DebugEventValue m_debugLevel;
		private readonly float m_spaceUiUtilitiesButton = 175;

		private readonly string[] m_templates = {
			"Assets/ScriptTemplates/82-CustomScript__TypeEvents__ComplexEvent-ComplexEvent.cs.txt",
			"Assets/ScriptTemplates/82-CustomScript__TypeEvents__ComplexEventInspector-ComplexEventInspector.cs.txt",
			"Assets/ScriptTemplates/82-CustomScript__TypeEvents__ComplexEventReceiver-ComplexEventReceiver.cs.txt",
			"Assets/ScriptTemplates/82-CustomScript__TypeEvents__ComplexEventReceiverInspector-ComplexEventReceiverInspector.cs.txt"
		};

		private bool m_advancedPanel;
		private string m_eventNameCreation = "Complex";
		private Type[] m_eventTypes;
		private Type[] m_receiverTypes;

		private Vector2 m_scrollPos;

		private int m_selectedType;
		private bool m_toggle = true;
		private bool m_toggleEventInspector;
		private bool m_toggleReceiver = true;
		private bool m_toggleReceiverInspector;

		private SerializedObject soThis;
		private SerializedProperty propEventsReferencedInScene;
		private SerializedProperty propREventsnotUsedInScene;

		private void OnEnable() {
			ScriptableObject target = this;
			soThis = new SerializedObject(target);
			propEventsReferencedInScene = soThis.FindProperty("m_referencedInScene");
			propREventsnotUsedInScene = soThis.FindProperty("m_notUsedInScene");
			
			//UnityEditor.SceneManagement.EditorSceneManager.sceneOpened += OnSceneOpenedCallback;
			//UnityEditor.SceneManagement.EditorSceneManager.sceneClosed += OnSceneClosedCallback;
			//UnityEditor.SceneManagement.EditorSceneManager.sceneSaved += OnSceneClosedCallback;
			
			m_debugLevel =
				AssetDatabase.LoadAssetAtPath<DebugEventValue>(
					AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("t:DebugEventValue")[0]));
			
			//RefreshEventsProjects();
			
		}

		private void OnDisable() {
			//UnityEditor.SceneManagement.EditorSceneManager.sceneOpened -= OnSceneOpenedCallback;
			//UnityEditor.SceneManagement.EditorSceneManager.sceneClosed -= OnSceneClosedCallback;
			//UnityEditor.SceneManagement.EditorSceneManager.sceneSaved -= OnSceneClosedCallback;
		}

		/* TOO HEAVY
		void OnSceneOpenedCallback(
			Scene _scene,
			UnityEditor.SceneManagement.OpenSceneMode _mode)
		{
			RefreshEventsProjects();
		}
		
		void OnSceneClosedCallback(
			Scene _scene)
		{
			RefreshEventsProjects();
		}
		*/

		private void OnGUI() {
			EditorGUIUtility.labelWidth = 75.0f;
			EditorGUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
			m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);
			EditorGUILayout.Separator();
			GUILayout.Label("ClapClapEvent Manager", EditorStyles.whiteLargeLabel);
			EditorGUILayout.Separator();
			FuncEditor.DrawUILine(Color.gray);
			
			if (GUILayout.Button("Refresh project's events")) {
				RefreshEventsProjects();
			}

			if (m_referencedInScene != null) {
				EditorGUILayout.PropertyField(propEventsReferencedInScene, true);
				soThis.Update();
			} else {
				EditorGUILayout.LabelField("No events used in scene.");
			}
			
			if (m_notUsedInScene != null) {
				EditorGUILayout.PropertyField(propREventsnotUsedInScene, true);
				soThis.Update();
			} else {
				EditorGUILayout.LabelField("All events used in scene.");
			}

			EditorGUILayout.Separator();
			GUILayout.Label("Utilities", EditorStyles.centeredGreyMiniLabel);
			EditorGUILayout.Separator();

			EditorGUILayout.BeginHorizontal();
			string[] options = m_eventTypes.Select(type => type.Name).ToArray();
			EditorGUILayout.LabelField("TypeEvent", GUILayout.ExpandWidth(false));
			m_selectedType = EditorGUILayout.Popup(m_selectedType, options);

			if (GUILayout.Button("Create event", GUILayout.Width(m_spaceUiUtilitiesButton))) {
				Object asset = CreateInstance(m_eventTypes[m_selectedType]);
				string folder_path_events =
					EditorUtility.SaveFilePanel("Folder of events",
												Application.dataPath + "/ClapClapEvent/Events",
												m_eventTypes[m_selectedType].Name, "asset");
				if (folder_path_events != "") {
					string asset_path =
						AssetDatabase.GenerateUniqueAssetPath(
							folder_path_events.Replace(Application.dataPath, "Assets"));
					AssetDatabase.CreateAsset(asset, asset_path);
					AssetDatabase.SaveAssets();
					Selection.activeObject = asset;
					m_selected = asset as EventClapClap;
				}
			}
			EditorGUILayout.EndHorizontal();
			FuncEditor.DrawUILine(Color.gray, 1);
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Event", GUILayout.ExpandWidth(false));
			m_selected =
				(EventClapClap) EditorGUILayout.ObjectField(m_selected, typeof(EventClapClap), false);
			GUI.enabled = !(m_selected == null || Selection.activeGameObject == null);

			if (GUILayout.Button("Add listener to selected", GUILayout.Width(m_spaceUiUtilitiesButton))) {
				Type event_type = m_selected.GetType();
				Type receiver_type = Type.GetType(event_type.FullName + "Receiver, " + event_type.Assembly, true);
				Component c = Selection.activeGameObject.GetComponent(receiver_type);
				if (c == null) {
					c = Selection.activeGameObject.AddComponent(receiver_type);
				}
				// ! Not safe code ! Don't change function names
				MethodInfo method = receiver_type.GetMethod("AddPersistentEventHandler");
				method.Invoke(c, new object[] {m_selected});
			}
			GUI.enabled = true;

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.Space(30);

			m_advancedPanel = EditorGUILayout.Foldout(m_advancedPanel, "Advanced", EditorStyles.foldoutHeader);
			if (m_advancedPanel) {
				
				
				EditorGUILayout.Separator();
				GUILayout.Label("Debug", EditorStyles.centeredGreyMiniLabel);
				EditorGUILayout.Separator();

				EditorGUILayout.BeginHorizontal();
				GUILayout.Label("DebugLevel");
				m_debugLevel.debugValue = (DebugLevel) EditorGUILayout.EnumPopup(m_debugLevel.debugValue);
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.Separator();

				GUILayout.Label("DebugColors");
				EditorGUILayout.Separator();

				EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Event log", EditorStyles.miniLabel);
				m_debugLevel.debugColors[0] =
					EditorGUILayout.ColorField(m_debugLevel.debugColors[0], GUILayout.Width(m_spaceUiUtilitiesButton));
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Receiver log", EditorStyles.miniLabel);
				m_debugLevel.debugColors[1] =
					EditorGUILayout.ColorField(m_debugLevel.debugColors[1], GUILayout.Width(m_spaceUiUtilitiesButton));
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Other log", EditorStyles.miniLabel);
				m_debugLevel.debugColors[2] =
					EditorGUILayout.ColorField(m_debugLevel.debugColors[2], GUILayout.Width(m_spaceUiUtilitiesButton));
				EditorGUILayout.EndHorizontal();
				
				EditorGUILayout.Separator();
				GUILayout.Label("Create typed event", EditorStyles.centeredGreyMiniLabel);
				EditorGUILayout.Separator();

				EditorGUIUtility.labelWidth = m_spaceUiUtilitiesButton;
				m_eventNameCreation = EditorGUILayout.TextField("Type", m_eventNameCreation);
				m_toggle = EditorGUILayout.Toggle("Event", m_toggle, GUILayout.Width(m_spaceUiUtilitiesButton));
				m_toggleEventInspector =
					EditorGUILayout.Toggle("Inspector", m_toggleEventInspector,
										   GUILayout.Width(m_spaceUiUtilitiesButton));
				m_toggleReceiver =
					EditorGUILayout.Toggle("Receiver", m_toggleReceiver, GUILayout.Width(m_spaceUiUtilitiesButton));
				m_toggleReceiverInspector = EditorGUILayout.Toggle("ReceiverInspector", m_toggleReceiverInspector,
																   GUILayout.Width(m_spaceUiUtilitiesButton));

				if (GUILayout.Button("Create TypedEvent")) {
					string folder_path_typeevents =
						EditorUtility.OpenFolderPanel("Folder of event's types",
													  Application.dataPath + "/ClapClapEvent/Scripts/CustomEvents", "");
					if (folder_path_typeevents != "") {
						if (m_toggle) {
							ScriptModificationProcessor.ScriptCreationFromTemplate(
								Path.Combine(folder_path_typeevents, m_eventNameCreation + "Event.cs"), m_templates[0]);
						}
						if (m_toggleEventInspector) {
							ScriptModificationProcessor.ScriptCreationFromTemplate(
								Path.Combine(folder_path_typeevents, "Editor/",
											 m_eventNameCreation + "EventInspector.cs"),
								m_templates[1]);
						}
						if (m_toggleReceiver) {
							ScriptModificationProcessor.ScriptCreationFromTemplate(
								Path.Combine(folder_path_typeevents, m_eventNameCreation + "EventReceiver.cs"),
								m_templates[2]);
						}
						if (m_toggleReceiverInspector) {
							ScriptModificationProcessor.ScriptCreationFromTemplate(
								Path.Combine(folder_path_typeevents, "Editor/",
											 m_eventNameCreation + "EventReceiverInspector.cs"), m_templates[3]);
						}
					}
				}
			}

			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndVertical();
		}

		private void RefreshEventsProjects() {
			m_referencedInScene = null;
			m_notUsedInScene = null;
			string[] guids = AssetDatabase.FindAssets("t:EventClapClap");
			List<EventVisualiser> events = new List<EventVisualiser>(guids.Length);
			List<EventClapClap> eventsNotUsed = new List<EventClapClap>(guids.Length);
			foreach (string guid in guids) {
				EventClapClap ev = AssetDatabase.LoadAssetAtPath<EventClapClap>(AssetDatabase.GUIDToAssetPath(guid));
				List<MonoBehaviour> all_references = UnityAssetUtilities.GetListSoReferencesInScene(ev);
				ILookup<bool, MonoBehaviour> is_listener = all_references.ToLookup(
					script => script.GetType().IsSubclassOf(typeof(EventReceiver)));
				if (is_listener[true].Count() != 0 || is_listener[false].Count() != 0) {
					events.Add(new EventVisualiser {
						m_ev = ev,
						m_listeners = is_listener[true].ToArray(),
						m_callers = is_listener[false].ToArray()
					});
				} else {
					eventsNotUsed.Add(ev);
				}
			}
			m_referencedInScene = events.ToArray();
			m_notUsedInScene = eventsNotUsed.ToArray();
			m_eventTypes = ReflectionUtilities.GetInheritedClasses(typeof(EventClapClap));
			m_receiverTypes = ReflectionUtilities.GetInheritedClasses(typeof(EventReceiver));
			soThis.Update();
			Repaint();
		}

		[MenuItem("Tools/ClapClapEvent/ManagerEditor")]
		public static void ShowWindow() {
			EditorWindow win = GetWindow(typeof(ManagerEventEditorWindow));
		}
	}
}
