namespace ClapClapEvent.Scripts.Editor.CustomEvents {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using System.Text;

	using Runtime;
	using Runtime.Utils;

	using UnityEditor;
	using UnityEditor.Events;

	using UnityEngine;
	using UnityEngine.Events;

	public static class UnityEventExtension {
		/// <summary>
		///     Auto register functions from the same game object on a Unity event.
		/// </summary>
		/// <param name="e">The event to register functions in.</param>
		/// <param name="caller">The caller Monobahaviour, the functions with the nameEvent will be discarded.</param>
		/// <param name="name">The name of the event, add "On" in script to this name for register.</param>
		public static void AutoReferenceEventUnity(this UnityEvent e, MonoBehaviour caller, string name) {
			List<MethodInfo> method_infos = caller.gameObject.GetMethods(
				typeof(void),
				null,
				ReflectionUtilities.bindFlags,
				new[] {caller});

			RegisterPersistentListeners(e, caller, method_infos, "On" + name);
			EditorUtility.SetDirty(caller);
		}

		/// <summary>
		///     Auto register functions from the same game object on a Unity event.
		///     Special case with one argument in callback.
		/// </summary>
		/// <typeparam name="T"> The generic type of the Unity Event</typeparam>
		/// <param name="e">The event to register functions in.</param>
		/// <param name="caller">The caller Monobahaviour, the functions with the nameEvent will be discarded.</param>
		/// <param name="name">The name of the event, add "On" in script to this name for register.</param>
		public static void AutoReferenceEventUnity<T>(this UnityEvent<T> e, MonoBehaviour caller, string name) {
			List<MethodInfo> method_infos = caller.gameObject.GetMethods(
				typeof(void),
				new[] {typeof(T)},
				ReflectionUtilities.bindFlags,
				new[] {caller});
			RegisterPersistentListeners(e, caller, method_infos, "On" + name);
			EditorUtility.SetDirty(caller);
		}

		/// <summary>
		///     Search and register events for EventScriptTrigger script.
		///     Use it in context menu.
		/// </summary>
		/// <param name="menuCommand">Argument for context menu.</param>
		[MenuItem("CONTEXT/EventCollisionFilteredTrigger/Auto-reference events reflection")]
		public static void AutoReferenceEventCollisionFilteredTrigger(MenuCommand menuCommand) {
			EventCollisionFilteredTrigger script = menuCommand.context as EventCollisionFilteredTrigger;

			//Search functions with CallbackContext as arguments (used only in input event).
			List<MethodInfo> method_infos = script.gameObject.GetMethods(typeof(void),
																		 new[] {typeof(GameObject)},
																		 ReflectionUtilities.bindFlags,
																		 new[] {script});

			//Filter and register events
			RegisterPersistentListeners(script.triggerFilteredEnter, script, method_infos, "OnTriggerFilteredEnter");
			RegisterPersistentListeners(script.triggerFilteredStay, script, method_infos, "OnTriggerFilteredStay");
			RegisterPersistentListeners(script.triggerFilteredExit, script, method_infos, "OnTriggerFilteredExit");

			EditorUtility.SetDirty(script);
		}

		private static void RegisterPersistentListeners(UnityEvent unity, MonoBehaviour caller,
														List<MethodInfo> method_infos, string nameFunc) {
			List<MethodInfo> methods = method_infos.Where(x => x.Name == nameFunc).ToList();
			if (methods.Count > 0) {
				StringBuilder sb = new StringBuilder();
				sb.Append("Registrations " + nameFunc + " from " + caller.gameObject + ".").Append("\n");
				foreach (MethodInfo info in methods) {
					UnityAction method_delegate =
						Delegate.CreateDelegate(typeof(UnityAction), caller.gameObject.GetComponent(info.DeclaringType),
												info) as UnityAction;
					UnityEventTools.RemovePersistentListener(unity, method_delegate);
					UnityEventTools.AddPersistentListener(unity, method_delegate);
					sb.Append(info.DeclaringType + "/" + info.Name + " function registred.").Append("\n");
				}
				Debug.Log(sb.ToString());
			} else {
				Debug.LogWarning("No functions " + nameFunc + " in " + caller.gameObject);
			}
		}

		private static void RegisterPersistentListeners<T>(UnityEvent<T> unity, MonoBehaviour caller,
														   List<MethodInfo> method_infos, string nameFunc) {
			List<MethodInfo> methods = method_infos.Where(x => x.Name.Equals(nameFunc)).ToList();
			if (methods.Count > 0) {
				StringBuilder sb = new StringBuilder();
				sb.Append("Registrations " + nameFunc + " from " + caller.gameObject + ".").Append("\n");
				foreach (MethodInfo info in methods) {
					UnityAction<T> method_delegate =
						Delegate.CreateDelegate(typeof(UnityAction<T>),
												caller.gameObject.GetComponent(info.DeclaringType),
												info) as UnityAction<T>;
					UnityEventTools.RemovePersistentListener(unity, method_delegate);
					UnityEventTools.AddPersistentListener(unity, method_delegate);
					sb.Append(info.DeclaringType + "/" + info.Name + " function registred.").Append("\n");
				}
				Debug.Log(sb.ToString());
			} else {
				Debug.LogWarning("No functions " + nameFunc + " in " + caller.gameObject);
			}
		}
	}
}