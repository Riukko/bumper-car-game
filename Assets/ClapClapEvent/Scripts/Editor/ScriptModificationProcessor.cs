namespace ClapClapEvent.Scripts.Editor.Utils {
	using System;
	using System.IO;

	using UnityEditor;

	using UnityEngine;

	/// <summary>
	///     Based on https://gist.github.com/wcoastsands/e2c0ee835ae1eb645bf0
	/// </summary>
	public class ScriptModificationProcessor : AssetModificationProcessor {
		public static void OnWillCreateAsset(string path) {
			path = path.Replace(".meta", "");
			int index = path.LastIndexOf(".");
			if (index != -1) {
				string extension = path.Substring(index);
				ScriptCreation(path, index, extension);
			}
		}

		/// <summary>
		///     To create script from code with a template.
		///     Template keywords will be later replace in OnWillCreateAsset.
		/// </summary>
		/// <param name="path">Path of the new script</param>
		/// <param name="pathTemplate">Path of the template.</param>
		public static void ScriptCreationFromTemplate(string path, string pathTemplate) {
			if (!File.Exists(path)) {
				File.Copy(pathTemplate, path);
			}
			AssetDatabase.Refresh();
			Debug.Log("Script created to " + path);
		}

		/// <summary>
		///     Replace Template keywords with information from the name and application.
		/// </summary>
		/// <param name="path">The path of the script.</param>
		/// <param name="index">Index of the '.' extension</param>
		/// <param name="extension">Extension</param>
		/// <exception cref="ArgumentOutOfRangeException">If index is negative.</exception>
		private static void ScriptCreation(string path, int index, string extension) {
			if (index <= 0) {
				throw new ArgumentOutOfRangeException(nameof(index));
			}

			if (extension != ".cs" && extension != ".js" && extension != ".boo") {
				return;
			}

			string filename = Path.GetFileName(path);

			index = Application.dataPath.LastIndexOf("Assets");
			path = Application.dataPath.Substring(0, index) + path;
			string file_text = File.ReadAllText(path);

			if (filename.Contains("Event")) {
				index = filename.LastIndexOf("Event");
				string type = filename.Substring(0, index);
				file_text = file_text.Replace("#TYPE#", type);
			}
			if (filename.Contains("Inspector")) {
				index = filename.LastIndexOf("Inspector");
				string object_name = filename.Substring(0, index);
				file_text = file_text.Replace("#OBJECT_INSPECTED#", object_name);
			}
			if (filename.Contains("Receiver")) {
				index = filename.LastIndexOf("Receiver");
				string event_name = filename.Substring(0, index);
				file_text = file_text.Replace("#EVENTNAME#", event_name);
			}

			file_text = file_text.Replace("#SCRIPTNAME#",
										  Path
											  .GetFileNameWithoutExtension(
												  filename)); // If not using default Unity creation
			file_text = file_text.Replace("#COMPANYNAMESPACE#", PlayerSettings.companyName);
			file_text = file_text.Replace("#PRODUCTNAMESPACE#", PlayerSettings.productName);

			File.WriteAllText(path, file_text);
			AssetDatabase.Refresh();
		}
	}
}