﻿namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	public delegate void StringEventCallback(string param);

	[CreateAssetMenu(fileName = "StringEvent", menuName = "Events/StringEvent")]
	public class StringEvent : ParametrisedEvent<string> { }
}