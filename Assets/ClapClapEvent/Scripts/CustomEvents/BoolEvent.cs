﻿namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	[CreateAssetMenu(fileName = "BoolEvent", menuName = "Events/BoolEvent")]
	public class BoolEvent : ParametrisedEvent<bool> { }
}