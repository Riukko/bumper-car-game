namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	public struct IntBool {
		public int first;
		public bool second;
	}

	[CreateAssetMenu(fileName = "IntBoolEvent", menuName = "Events/IntBoolEvent")]
	public class IntBoolEvent : ParametrisedEvent<IntBool> { }
}