namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	public struct PairInt {
		public int first;
		public int second;
	}

	[CreateAssetMenu(fileName = "PairIntEvent", menuName = "Events/PairIntEvent")]
	public class PairIntEvent : ParametrisedEvent<PairInt> { }
}