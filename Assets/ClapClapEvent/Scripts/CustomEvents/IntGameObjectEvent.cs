namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;
	
	public struct IntGameObject
	{
		public int first;
		public GameObject second;
	}

	[CreateAssetMenu(fileName = "IntGameObjectEvent", menuName = "Events/IntGameObjectEvent")]
	public class IntGameObjectEvent : ParametrisedEvent<IntGameObject> { }
}