namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	[CreateAssetMenu(fileName = "IntEvent", menuName = "Events/IntEvent")]
	public class IntEvent : ParametrisedEvent<int> { }
}