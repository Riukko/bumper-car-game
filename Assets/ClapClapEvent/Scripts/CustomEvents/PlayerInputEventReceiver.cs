using UnityEngine.InputSystem;

namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	public class PlayerInputEventReceiver : MultiParamEventReceiver<PlayerInput> { }
}
