﻿namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	public delegate void GameObjectEventCallback(GameObject param);

	[CreateAssetMenu(fileName = "GameObjectEvent", menuName = "Events/GameObjectEvent")]
	public class GameObjectEvent : ParametrisedEvent<GameObject> { }
}