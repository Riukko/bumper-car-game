namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	public class Vector2EventReceiver : MultiParamEventReceiver<Vector2> { }
}