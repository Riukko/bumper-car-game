﻿namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	public class GameObjectEventReceiver : MultiParamEventReceiver<GameObject> { }
}