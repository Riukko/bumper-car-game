namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	public class Vector3EventReceiver : MultiParamEventReceiver<Vector3> { }
}