using UnityEngine.InputSystem;

namespace ClapClapEvent.Scripts.Runtime.CustomEvents.Imp {
	using UnityEngine;

	[CreateAssetMenu(fileName = "PlayerInputEvent", menuName = "Events/PlayerInputEvent")]
	public class PlayerInputEvent : ParametrisedEvent<PlayerInput> { }
}
