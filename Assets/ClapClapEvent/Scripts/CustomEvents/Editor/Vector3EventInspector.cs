namespace ClapClapEvent.Scripts.Editor.CustomEvents.Inspectors {
	using Runtime.CustomEvents.Imp;

	using UnityEditor;

	using UnityEngine;

	[CustomEditor(typeof(Vector3Event), true), CanEditMultipleObjects]
	public class Vector3EventInspector : Editor {
		private Vector3Event[] m_objs;
		private Vector3 m_parameter;

		public void OnEnable() {
			//Multi object management
			Object[] target_cast = targets;
			m_objs = new Vector3Event[targets.Length];
			int i = 0;
			foreach (Object o in targets) {
				m_objs[i] = (Vector3Event) o;
				i++;
			}
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();
			m_parameter = EditorGUILayout.Vector3Field("Parameter :", m_parameter);
			EditorGUILayout.Separator();
			if (GUILayout.Button("Invoke event")) {
				foreach (Vector3Event e in m_objs) {
					e.Invoke(m_parameter);
				}
			}
		}
	}
}