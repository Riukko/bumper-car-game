namespace ClapClapEvent.Scripts.Editor.CustomEvents.Inspectors {
	using Runtime.CustomEvents.Imp;

	using UnityEditor;

	using UnityEngine;

	[CustomEditor(typeof(BoolEvent), true), CanEditMultipleObjects]
	public class BoolEventInspector : Editor {
		private BoolEvent[] m_objs;
		private bool m_parameter;

		public void OnEnable() {
			//Multi object management
			Object[] target_cast = targets;
			m_objs = new BoolEvent[targets.Length];
			int i = 0;
			foreach (Object o in targets) {
				m_objs[i] = (BoolEvent) o;
				i++;
			}
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();
			m_parameter = EditorGUILayout.Toggle("Parameter :", m_parameter);
			EditorGUILayout.Separator();
			if (GUILayout.Button("Invoke event")) {
				foreach (BoolEvent e in m_objs) {
					e.Invoke(m_parameter);
				}
			}
		}
	}
}