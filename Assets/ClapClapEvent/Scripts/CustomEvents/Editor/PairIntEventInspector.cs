namespace ClapClapEvent.Scripts.Editor.CustomEvents.Inspectors {
	using Runtime.CustomEvents.Imp;

	using UnityEditor;

	using UnityEngine;

	[CustomEditor(typeof(PairIntEvent), true), CanEditMultipleObjects]
	public class PairIntEventInspector : Editor {
		private PairIntEvent[] m_objs;
		private PairInt m_parameter;

		public void OnEnable() {
			//Multi object management
			Object[] target_cast = targets;
			m_objs = new PairIntEvent[targets.Length];
			int i = 0;
			foreach (Object o in targets) {
				m_objs[i] = (PairIntEvent) o;
				i++;
			}
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();
			m_parameter.first = EditorGUILayout.IntField("Parameter 1:", m_parameter.first);
			m_parameter.second = EditorGUILayout.IntField("Parameter 2:", m_parameter.second);
			EditorGUILayout.Separator();
			if (GUILayout.Button("Invoke event")) {
				foreach (PairIntEvent e in m_objs) {
					e.Invoke(m_parameter);
				}
			}
		}
	}
}