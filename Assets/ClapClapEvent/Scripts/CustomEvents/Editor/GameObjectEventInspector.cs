namespace ClapClapEvent.Scripts.Editor.CustomEvents.Inspectors {
	using Runtime.CustomEvents.Imp;

	using UnityEditor;

	using UnityEngine;

	[CustomEditor(typeof(GameObjectEvent), true), CanEditMultipleObjects]
	public class GameObjectEventInspector : Editor {
		private GameObjectEvent[] m_objs;
		private GameObject m_parameter;

		public void OnEnable() {
			//Multi object management
			Object[] target_cast = targets;
			m_objs = new GameObjectEvent[targets.Length];
			int i = 0;
			foreach (Object o in targets) {
				m_objs[i] = (GameObjectEvent) o;
				i++;
			}
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();
			m_parameter =
				(GameObject) EditorGUILayout.ObjectField("Parameter :", m_parameter, typeof(GameObject), true);
			EditorGUILayout.Separator();
			if (GUILayout.Button("Invoke event")) {
				foreach (GameObjectEvent e in m_objs) {
					e.Invoke(m_parameter);
				}
			}
		}
	}
}