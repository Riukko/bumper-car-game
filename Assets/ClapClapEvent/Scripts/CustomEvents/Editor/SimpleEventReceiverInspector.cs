namespace ClapClapEvent.Scripts.Editor.CustomEvents.Inspectors {
	using System.Collections.Generic;
	using System.Linq;

	using Runtime.CustomEvents;

	using UnityEditor;

	using UnityEngine;

	using Utils;

	[CustomEditor(typeof(SimpleEventReceiver), true), CanEditMultipleObjects]
	public class SimpleEventReceiverInspector : Editor {
		private SerializedProperty m_listeners;

		public void OnEnable() {
			m_listeners = serializedObject.FindProperty("m_listeners");
		}

		public override void OnInspectorGUI() {
			serializedObject.Update();
			if (!serializedObject.isEditingMultipleObjects) { // Edition single
				EditorGUILayout.PropertyField(m_listeners);
			} else {
				EditorGUILayout.LabelField("Listeners are not editable with multiple objects editing.");
			}

			FuncEditor.DrawUILine(Color.gray);

			if (GUILayout.Button("Auto-register all")) {
				int i = 0;
				foreach (List<EventListener> lists in m_listeners.GetTargetsField<List<EventListener>>()) {
					MonoBehaviour script = m_listeners.serializedObject.targetObjects[i] as MonoBehaviour;
					foreach (EventListener e in lists.Where(e => e.m_ev != null)) {
						e.m_func.AutoReferenceEventUnity(script, e.m_ev.GetEventName());
					}
					i++;
				}
			}

			serializedObject.ApplyModifiedProperties();
		}
	}
}