# ClapClapEvent

An event system for Unity.

Permit to handle events in your projects that are :
- Persistent
- Cross-Scene
- With parameters
- Easy to iterate

The main selling point is that in order to test you don't need that your events are called from code, or that a listener is linked to your events.
Really adapted if you use an Additive Scenes workflow.

## Installation
---

Import the UnityPackage in your project and it's done.

## Quick use
---

Open the editor in Tools > ClapClapEvent > ManagerEditor.

Here you see the events created in "Project Events", with callers and listeners associated if they have.

You can create events, and add listeners to GameObjects selected in scene.
For instance, in order to create a event that activate/deactivate object that are listenning, select a "BoolEvent" for the type and click on CreateEvent, choose the name of the Event, like "DeactivateEvent" (watchout the name suffix must be "Event" in order to have some utilities ) and Click after that on the GameObject you want to listen. Click on "Add listener to selected" in order to add the event (that have been set on the Event field).
Finnally, go on the Inspector and on the BoolEventReceiver, add a callback on the UnityEvent to GameObject>SetActive. You can see the difference between dynamic and static parameter.

## Events and Receiver
---

You can create Events from two ways, from the context menu in hierarchy Create>Events or from the Editor.

The created Events are strictly typed and only can be associated with the receiver of the same type.

You can add the event to receiver from the ManagerEditor, but also from the Inspector by drag and drop in the listeners list, or with the "AddEvent" button (from the field it don't find the created events due to Unity limitations).

Than you can register local functions automatically if they respect the convention name of event (or a warning will be trigger), for instance, ALongNameEvent will register OnALongName functions on the GameObject, the parameters of the function must also be the same as the Event (special case of SimpleEvent with no parameters).
Otherwise you can register manually the functions from UnityEvent controls.


## Advanced
---
### Debug

You can change the debug level from the ManagerEditor, by default it's set on arning. You can also change the colors for debugs.

### Architecture

```plantuml
@startuml
Caller --> Event
Event ..> Receiver_1
Event ..> ...
Receiver_1 --> UnityEvent

UnityEvent --> Callback_1
UnityEvent --> Callback_X
@enduml
```

### Create custom type event

In order to create your own types, you can use in the editor the advanced section that permit also to create default inspectors with it if you want. Type the type of event you want and modify the files created in order to suit your needs.
