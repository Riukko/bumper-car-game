# Kitty-Kat Boom

Lien itch :
https://sushichat.itch.io/kitty-kat-boom

Kitty-Kat Boom is a third-person versus racing game, developed in 15 days during the nano projects at Cnam Enjmin. You play as a cute little cat, pilot of a fish-rocket. Your mission: blow up your adversary. 

Players start back to back and race in opposite directions. During the first part of the circuit, players accumulate as much gas as possible, as measured by a number of points, by passing over three different boosts. When they reach the confrontation road, the players can collide (BOOM !) or avoid each other.

# Credits

Game & Level Designers : 
Léo BOUSSOUIRA-TERRISSE & Zoé SEMPÉ

Programmers :
Marc CERUTTI & Ilan OUAMROUCHE

Artists : 
Johanna BIRRALDACCI & Astrid MERIT

Sound designer & Composer : 
Gabriel SAUVAGE
Special thanks to Thomas CHASTAGNOL (P15)

UX Designer & UR  : 
Christopher NORTON

UI Designer : 
Zoé SEMPÉ

Project Manager :
Eve-Marie TISSIER
